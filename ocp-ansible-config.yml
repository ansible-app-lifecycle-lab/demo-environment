---
- name: Create AAP organizations
  ansible.controller.organization:
    name: "{{ item }}"
    description: "{{ item }} org"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
  loop: "{{ lab_users_list }}"

- name: Create AAP teams
  ansible.controller.team:
    name: "{{ item.name }}"
    description: "{{ item.desc }}"
    organization: "{{ item.org }}"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
  loop: "{{ teams }}"
  when:
    - teams is defined
    - teams|length > 0

- name: Create AAP Users
  ansible.controller.user:
    username: "{{ item }}"
    password: "{{ item }}"
    email: "{{ item }}@rhte.com"
    update_secrets: false
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
  loop: "{{ lab_users_list }}"

- name: Add AAP Users to organization 
  ansible.controller.role:
    user: "{{ item }}"
    role: "admin"
    organization: "{{ item }}"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
  loop: "{{ lab_users_list }}"

- name: Create AAP admin Users
  ansible.controller.user:
    username: "{{ item.username }}"
    password: "{{ item.password }}"
    email: "{{ item.email }}"
    update_secrets: false
    superuser: yes
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
  loop: "{{ admin_users }}"
  when:
    - admin_users is defined
    - admin_users|length > 0

- name: Create AAP auditor Users
  ansible.controller.user:
    username: "{{ item.username }}"
    password: "{{ item.password }}"
    email: "{{ item.email }}"
    update_secrets: false
    auditor: yes
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
  loop: "{{ auditor_users }}"
  when:
    - auditor_users is defined
    - auditor_users|length > 0

- name: Create AAP Inventories
  ansible.controller.inventory:
    name: "{{ item.0.name }}"
    description: "{{ item.0.desc }}"
    organization: "{{ item.1 }}"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ item.1 }}"
    controller_password: "{{ item.1 }}"
  loop: "{{ inventories|product(lab_users_list)|list }}"
  when:
    - inventories is defined
    - inventories|length > 0

- name: Add Hosts
  ansible.controller.host:
    name: "{{ item.0.name }}"
    description: "{{ item.0.desc }}"
    inventory: "{{ item.0.inv }}"
    variables: "{{ item.0.vars }}"
    controller_host: "{{ aap_host }}"
    controller_username: "{{ item.1 }}"
    controller_password: "{{ item.1 }}"
    state: present
    validate_certs: no
  loop: "{{ hosts|product(lab_users_list)|list }}"
  when:
    - hosts is defined
    - hosts|length > 0

- name: Add Execution Enviroment
  ansible.controller.execution_environment:
    name: "{{ item.name }}"
    image: "{{ item.desc }}"
    state: present
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
    validate_certs: no
  loop: "{{ execution_enviroment }}"
  when:
    - execution_enviroment is defined
    - execution_enviroment|length > 0

- name: Create Custom Credential Types
  ansible.controller.credential_type:
    name: "{{ item.name }}"
    kind: "{{ item.kind }}"
    inputs: "{{ item.inputs }}"
    injectors: "{{ item.injectors }}"
    state: present
    validate_certs: false
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
  loop: "{{ scm_custom_credential_types }}"
  when:
    - scm_custom_credential_types is defined
    - scm_custom_credential_types|length > 0

- name: Create Credentials
  ansible.controller.credential:
    name: "{{ item.0.name }}"
    description: "{{ item.0.desc }}"
    organization: "{{ item.1 }}"
    credential_type: "{{ item.0.type }}"
    update_secrets: false
    inputs: "{{ item.0.inputs }}"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ item.1 }}"
    controller_password: "{{ item.1 }}"
  loop: "{{ scm_credentials|product(lab_users_list)|list }}"
  when:
    - scm_credentials is defined
    - scm_credentials|length > 0

- name: Update OpenShift API Bearer Token Credentials for lab users
  ansible.controller.credential:
    name: "OpenShift API Bearer Token"
    description: "OpenShift API Bearer Token"
    organization: "{{ item }}"
    credential_type: "OpenShift or Kubernetes API Bearer Token"
    update_secrets: true
    inputs:
      host: "{{ openshift_api }}"
      bearer_token: "{{ oauth_token_dict[item] }}"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ item }}"
    controller_password: "{{ item }}"
  loop: "{{ lab_users_list }}"

- name: Create SCM Projects
  ansible.controller.project:
    name: "{{ item.0.name }}"
    description: "{{ item.0.desc }}"
    organization: "{{ item.1 }}"
    scm_branch: "{{ item.0.git_branch }}"
    scm_clean: yes
    #scm_credential: "{{ item.git_cred }}"
    scm_delete_on_update: yes
    scm_type: git
    scm_update_on_launch: "{{ item.0.update_on_launch }}"
    scm_url: "http://gitea.{{ ocp_ingress_domain[0] }}/{{ item.1 }}/helm-ansible-pipeline"
    state: present
    wait: yes
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ item.1 }}"
    controller_password: "{{ item.1 }}"
  loop: "{{ scm_projects|product(lab_users_list)|list }}"
  when:
    - scm_projects is defined
    - scm_projects|length > 0

- name: Create AAP Job Templates
  ansible.controller.job_template:
    name: "{{ item.0.name }}"
    job_type: "run"
    organization: "{{ item.1 }}"
    inventory: "{{ item.0.inv }}"
    project: "{{ item.0.project }}"
    playbook: "{{ item.0.playbook }}"
    credentials: "{{ item.0.creds }}"
    execution_environment: "{{ item.0.execution_environment }}"
    ask_variables_on_launch: "{{ item.0.ask_variables_on_launch }}"
    allow_simultaneous: "{{ item.0.allow_simultaneous }}"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ item.1 }}"
    controller_password: "{{ item.1 }}"
  loop: "{{ scm_jobtemplates|product(lab_users_list)|list }}"
  when:
    - scm_jobtemplates is defined
    - scm_jobtemplates|length > 0

- name: Create AAP Job Templates with Survey
  ansible.controller.job_template:
    name: "{{ item.name }}"
    job_type: "run"
    organization: "{{ item.org }}"
    inventory: "{{ item.inv }}"
    project: "{{ item.project }}"
    playbook: "{{ item.playbook }}"
    credentials: "{{ item.creds }}"
    execution_environment: "{{ item.execution_environment }}"
    survey_enabled: "{{ item.survey_enabled }}"
    survey_spec: "{{ item.survey_spec }}"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
  loop: "{{ scm_jobtemplates_survey }}"
  when:
    - scm_jobtemplates_survey is defined
    - scm_jobtemplates_survey|length > 0

- name: Create AAP Workflow Job Templates
  ansible.controller.workflow_job_template:
    name: "{{ item.0.0.name }}"
    organization: "{{ item.1 }}"
    inventory: "{{ item.0.0.inv }}"
    schema:
      - identifier: "{{ item.0.1.identifier }}"
        unified_job_template:
          organization:
            name: "{{ item.1 }}"
          name: "{{ item.0.1.unified_job_template.name }}"
          type: "{{ item.0.1.unified_job_template.type }}"
        credentials: "{{ item.0.1.credentials }}"
        related:
          success_nodes: "{{ item.0.1.related.success_nodes }}"
          failure_nodes: "{{ item.0.1.related.failure_nodes }}"
          always_nodes: "{{ item.0.1.related.always_nodes }}"
          credentials: "{{ item.0.1.related.credentials }}"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ item.1 }}"
    controller_password: "{{ item.1 }}"
  loop: "{{ scm_workflowjobtemplates|subelements('steps')|product(lab_users_list)|list }}"
  when:
    - scm_workflowjobtemplates is defined
    - scm_workflowjobtemplates|length > 0

- name: Create AAP Workflow Job Templates with Survey
  ansible.controller.workflow_job_template:
    name: "{{ item.name }}"
    organization: "{{ item.org }}"
    inventory: "{{ item.inv }}"
    schema: "{{ item.steps }}"
    survey_enabled: "{{ item.survey_enabled }}"
    survey_spec: "{{ item.survey_spec }}"
    state: present
    validate_certs: no
    controller_host: "{{ aap_host }}"
    controller_username: "{{ aap_user }}"
    controller_password: "{{ aap_pass }}"
  loop: "{{ scm_workflowjobtemplates_survey }}"
  when:
    - scm_workflowjobtemplates_survey is defined
    - scm_workflowjobtemplates_survey|length > 0
