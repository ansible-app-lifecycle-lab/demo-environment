# demo-environment

Ansible APP Lifecycle Lab

## Demo/Lab Developers:

*Carlos Lopez Bartolome*, Specialist Solution Architect, Red Hat

*Cesar Fernandez*, EMEA Ansible Specialist Solution Architect, Red Hat

## Prerequisites

- Slack App.
- OpenShift Cluster +4.12 with admin rights.
- Ansible Navigator.
- Demo Environment project.

### Slack App

You can create a Slack App in the following [link](https://aapdemo.slack.com/apps).

### OpenShift

If you don't have an OpenShift Cluster 4.12 with admin rights, you can request one in the following [link](https://demo.redhat.com/catalog?category=Workshops&item=babylon-catalog-prod%2Fsandboxes-gpte.ocp412-wksp.prod).

### Ansible Navigator

If you have Linux, you can install the `ansible-navigator` tool following the steps described in the following [link](https://ansible-navigator.readthedocs.io/installation/#linux).

> Note: Run the command `ansible-navigator --version` to make sure it was installed correctly.

## How to deploy the demo environment

### Demo Environment Project

- Clone the demo environment project:

```sh
cd ~
git clone https://gitlab.com/ansible-app-lifecycle-lab/demo-environment.git
```

### Demo Environment Deployment

Follow the steps described bellow to deploy the demo environment:

- Create the lab configuration variables file and replace the highlighted variables:

```sh
cat << EOF > demo-environment/vars/lab-config.yml
---
# Demo vars
app_lifecycle_lab_namespace: app-lifecycle-lab
app_lifecycle_lab_templates_path: templates

# OCP vars
ocp_templates_path: "{{ app_lifecycle_lab_templates_path }}/images"

# Gitea vars
gitea_templates_path: "{{ app_lifecycle_lab_templates_path }}/gitea"

# Nexus vars
nexus_templates_path: "{{ app_lifecycle_lab_templates_path }}/nexus"

# OpenShift GitOps vars
gitops_operator_templates_path: "{{ app_lifecycle_lab_templates_path }}/gitops/operator"
gitops_config_templates_path: "{{ app_lifecycle_lab_templates_path }}/gitops/config"

# Etherpad vars
etherpad_templates_path: "{{ app_lifecycle_lab_templates_path }}/etherpad"

# Slack vars
slack_token: "<SLACK_TOKEN>"
slack_channel: "#<SLACK_CHANNEL>"

# Ansible vars
ansible_operator_templates_path: "{{ app_lifecycle_lab_templates_path }}/ansible/operator"
ansible_instance_templates_path: "{{ app_lifecycle_lab_templates_path }}/ansible/instance"
ansible_instance_admin_password: redhat
ansible_instance_manifest: "<AAP_MANIFEST>" --> !!!ENCODE IN BASE64!!!
EOF
```

- Deploy the demo environment:

```sh
cd ~/demo-environment/ansible-navigator
ansible-navigator run ../ocp-demo-deploy.yml -m stdout \
  -e 'ansible_python_interpreter=/usr/bin/python3' \
  -e 'openshift_api=<OPENSHIFT_URL>' \
  -e 'openshift_token=<OPENSHIFT_API>' \
  -e 'openshift_storage_class=<OPENSHIFT_STORAGE_CLASS>' \
  -e 'lab_size=medium' \
  -e 'lab_action=deploy'
```
